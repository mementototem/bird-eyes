package net.fingersports.android.birdeyes;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class LoginActivity extends Activity {
	private final static boolean LOG = Const.LOG;
	
	private static boolean returnFromTwitter = false;
	
	private static SharedPreferences mPref;
	private String mTwitterToken;
	private String mTwitterSecret;
	private static RequestToken mTwitterRequest;
	private static Twitter mTwitter; 
	private AccessToken mAccessToken;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		mPref = PreferenceManager.getDefaultSharedPreferences(this);
		mTwitterToken = mPref.getString(Const.KEY_TOKEN, null);
		mTwitterSecret = mPref.getString(Const.KEY_SECRET, null);
		
		// Run when return from Twitter's OAuth page
		if (mTwitterToken == null || mTwitterSecret == null) {
			Uri uri = getIntent().getData();
			if (uri != null && uri.toString().startsWith(Const.API_CALLBACK)) {
				final String verifier = uri.getQueryParameter(Const.API_VERIFIER);
				returnFromTwitter = true;
				
				try {
					Thread runner = new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								LoginActivity.this.mAccessToken = mTwitter.getOAuthAccessToken(mTwitterRequest, verifier);
								saveToken();
							}
							catch (TwitterException e) {
								if (LOG) Log.e("TWITTER", e.getErrorMessage());
							}
						}
					});
					
					runner.start();
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else {
			startMainActivity();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if  (returnFromTwitter) {
			displayButton(Const.LOGIN_LOAD_DATA);
		}
		else { 
			checkConnection();
		}
	}
	
	
	protected void saveToken() {
		mTwitterToken = mAccessToken.getToken();
		mTwitterSecret = mAccessToken.getTokenSecret();
		
		//if (LOG) Log.i("TWITTER", "Token: " + mTwitterToken);
		//if (LOG) Log.i("TWITTER", "Secret: " + mTwitterSecret);
		
		SharedPreferences.Editor editor = mPref.edit();
		editor.putString(Const.KEY_TOKEN, mTwitterToken);
		editor.putString(Const.KEY_SECRET, mTwitterSecret);
		editor.commit();
		
		startMainActivity();
	}
	
	public void onTryAgainClick(View v) {
		checkConnection();
	}
	
	public void onLoginClick(View v) {
		mTwitter = new TwitterFactory().getInstance();
		mTwitter.setOAuthConsumer(Const.API_KEY, Const.API_SECRET);
		mTwitterRequest = null;
		
		Thread runner = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					mTwitterRequest = mTwitter.getOAuthRequestToken(Const.API_CALLBACK);
					Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(mTwitterRequest.getAuthenticationURL()));
					LoginActivity.this.startActivity(i);
				}
				catch (TwitterException e) {
					Log.e("TWITTER", e.getErrorMessage());
				}
			}
		});
		
		runner.start();
	}
	
	private void checkConnection() {
		if (Loader.isConnected(this)) {
			if (mTwitterToken == null || mTwitterSecret == null) {
				displayButton(Const.LOGIN_HAVE_CONNECTION);
			}
			else {
				startMainActivity();
			}
		}
		else {
			displayButton(Const.LOGIN_NO_CONNECTION);
		}
	}
	
	/**
	 * Show/Hide Login button, Try button and No Connection message on activity
	 * @param val true, if want to display Login button
	 */
	private void displayButton(int state) {
		Button loginButton = (Button) findViewById(R.id.login);
		LinearLayout tryLayout = (LinearLayout) findViewById(R.id.try_layout);
		LinearLayout loadingLayout = (LinearLayout) findViewById(R.id.loading_layout);
		
		switch (state) {
		case Const.LOGIN_HAVE_CONNECTION:
			loginButton.setVisibility(View.VISIBLE);
			tryLayout.setVisibility(View.INVISIBLE);
			loadingLayout.setVisibility(View.INVISIBLE);
			break;
			
		case Const.LOGIN_LOAD_DATA:
			loginButton.setVisibility(View.INVISIBLE);
			tryLayout.setVisibility(View.INVISIBLE);
			loadingLayout.setVisibility(View.VISIBLE);
			break;
			
		case Const.LOGIN_NO_CONNECTION:
			loginButton.setVisibility(View.INVISIBLE);
			tryLayout.setVisibility(View.VISIBLE);
			loadingLayout.setVisibility(View.INVISIBLE);
			break;
		}
	}
	
	private void startMainActivity() {
		Intent main = new Intent(this, MainActivity.class);
		this.startActivity(main);
		this.finish();
	}
}

package net.fingersports.android.birdeyes;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.util.LruCache;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class MainActivity extends Activity {
    private final static boolean LOG = Const.LOG;
    private final static String TAG = "MAIN";

    private static SharedPreferences mPref;

    private static Twitter mTwitter;
    private String mTwitterToken;
    private String mTwitterSecret;
    private AccessToken mAccessToken;

    private final CopyOnWriteArrayList<Status> mTweets = new CopyOnWriteArrayList<Status>();
    private TweetAdapter mTweetsAdapter;
    private ListView mTweetsList;

    private LruCache<String, Bitmap> mAvatarCache;
    private LruCache<Long, Bitmap> mImageCache;
    private static File mAvatarCacheDir;
    private static File mImageCacheDir;

    private static int mSampleSize = 1;

    private View mLoadingFooter;
    private View mLoadingView;

    private static ThreadPoolExecutor mExecutor;
    private final BlockingQueue<Runnable> mQueue = new LinkedBlockingQueue<Runnable>();
    private static Thread mTweetLoader;
    private static TweetsLoaderService mTweetsLoaderService;

    private boolean mTweetsIsLoading = false;
    private boolean mHaveMore = true;
    private boolean mFirstTime = true;
    private static int mHaveTwitterApp = Const.NOT_CHECK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mPref = PreferenceManager.getDefaultSharedPreferences(this);
        mTwitterToken = mPref.getString(Const.KEY_TOKEN, null);
        mTwitterSecret = mPref.getString(Const.KEY_SECRET, null);

        if (!isLoggedIn()) {
            return;
        }

        if (mTwitter == null) {
            setupTwitter();
        }

        if (mAvatarCache == null || mImageCache == null) {
            setupCache();
        }

        setupSampleSize();

        File mCacheDir = getExternalCacheDir();
        if (mCacheDir == null) {
            // if external storage not available, fall back to internal storage
            mCacheDir = getCacheDir();
        }

        mAvatarCacheDir = new File(mCacheDir.getAbsolutePath() + "/avatar/");
        mImageCacheDir = new File(mCacheDir.getAbsolutePath() + "/image/");

        if (!mAvatarCacheDir.isDirectory()) {
            mAvatarCacheDir.mkdirs();
        }

        if (!mImageCacheDir.isDirectory()) {
            mImageCacheDir.mkdirs();
        }

        if (LOG) Log.i(TAG, "CPU Cores: " + Const.NUM_OF_CORES);

        mExecutor = new ThreadPoolExecutor(Const.NUM_OF_CORES, Const.NUM_OF_CORES, Const.KEEP_ALIVE_TIME, Const.KEEP_ALIVE_TIME_UNIT, mQueue);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);

        mTweetsList = (ListView) findViewById(R.id.list_statuses);

        mLoadingFooter = LayoutInflater.from(this).inflate(R.layout.part_loading, null);

        mLoadingView = findViewById(R.id.main_load_layout);

        mTweetsAdapter = new TweetAdapter(this, R.layout.part_status, mTweets, mAvatarCache, mImageCache, mAvatarCacheDir, mImageCacheDir, mExecutor);
        mTweetsList.setAdapter(mTweetsAdapter);

        mTweetsLoaderService = new TweetsLoaderService(this, mTwitter, mTweets);

        mTweetsList.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean fewLeft = firstVisibleItem + visibleItemCount >= totalItemCount - Const.LOAD_MORE_THRESHOLD;

                if (!mTweetsIsLoading && mHaveMore && fewLeft) {
                    mTweetsIsLoading = true;
                    mTweetLoader = new Thread(mTweetsLoaderService);
                    mTweetLoader.start();
                }

                if (!mHaveMore) {
                    mTweetsList.removeFooterView(mLoadingFooter);
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // ignore
            }

        });

        mTweetsList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                if (position >= mTweets.size() ) {
                    if (!mTweetsIsLoading) {
                        mTweetLoader = new Thread(mTweetsLoaderService);
                        mTweetLoader.start();
                    }
                    return;
                }

                long tweetId = mTweets.get(position).getId();
                String name = mTweets.get(position).getUser().getScreenName();

                if (LOG) Log.i(TAG, "Tweet ID: " + tweetId);

                gotoTwitterApp(name, tweetId);

            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //testing();
    }

    @Override
    protected void onDestroy() {
        mExecutor.shutdown();

        super.onDestroy();
    }

    private boolean isLoggedIn() {
        return mTwitterToken != null && mTwitterSecret != null;
    }

    private void setupTwitter() {
        mTwitter = TwitterFactory.getSingleton();
        mTwitter.setOAuthConsumer(Const.API_KEY, Const.API_SECRET);
        mAccessToken = new AccessToken(mTwitterToken, mTwitterSecret);
        mTwitter.setOAuthAccessToken(mAccessToken);
    }

    private void setupCache() {
        final int maxMemSize = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        final int avatarSize = 1024 * 1024 * maxMemSize / 16;
        final int imageSize = 1024 * 1024 * maxMemSize / 4;
        if (LOG) Log.i(TAG, "max mem size: " + maxMemSize + ", avatar: " + avatarSize + ", image: " + imageSize);

        if (mAvatarCache == null) {
            mAvatarCache = new LruCache<>(avatarSize);
        }

        if (mImageCache == null) {
            mImageCache = new LruCache<>(imageSize);
        }
    }

    private void setupSampleSize() {
        // if screen small than image size scale image down
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        int selected;

        selected = width < height ? height : width;

        if (selected < Const.DEFAULT_PIC_SIZE) {
            mSampleSize = Const.DEFAULT_PIC_SIZE / selected;
        }

        if (LOG) Log.i(
                TAG,
                String.format(
                        Locale.US,
                        "screen size: %dx%d, selected: %d, sample size: %d",
                        width, height, selected, mSampleSize));
    }

    public void doAfterLoadedNewTweets() {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mFirstTime) {
                    mFirstTime = false;
                    mLoadingView.setVisibility(View.GONE);
                    mLoadingView = null;

                    mTweetsList.addFooterView(mLoadingFooter);
                }

                mTweetsAdapter.notifyDataSetChanged();
            }
        });

        mTweetsIsLoading = false;
    }

    private void gotoTwitterApp(String name, long id) {
        String uri;

        if(checkTwitterApp()) {
            uri = Const.API_NATIVE_URI + id;
        }
        else {
            uri = String.format(Locale.US, Const.API_BROWSE_URI, name, id);
        }

        final Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        this.startActivity(i);
    }

    private boolean checkTwitterApp() {
        if (mHaveTwitterApp == Const.HAVE_TWITTER_APP) {
            return true;
        }

        if (mHaveTwitterApp == Const.NO_TWITTER_APP) {
            return false;
        }

        PackageManager pm = getPackageManager();

        final Intent t = new Intent(Intent.ACTION_VIEW, Uri.parse(Const.API_NATIVE_URI + 0));
        List<ResolveInfo> list = pm.queryIntentActivities(t, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            mHaveTwitterApp = Const.HAVE_TWITTER_APP;
            return true;
        }

        mHaveTwitterApp = Const.NO_TWITTER_APP;
        return false;
    }

    @SuppressWarnings("unused")
    private void testing() {

        Thread runner = new Thread(new Runnable() {

            @Override
            public void run() {
                // lower priority
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            }

        });

        runner.start();

    }
}

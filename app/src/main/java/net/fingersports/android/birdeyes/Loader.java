package net.fingersports.android.birdeyes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class Loader {
	private final static boolean LOG = Const.LOG;
	private final static String TAG = "Loader";
	
	private final static int TIME_OUT = 5000;
	
	public static Bitmap loadImage(String url, String name, File dir) {
		Bitmap bmp = loadImageFromStorage(dir, name, 1);

		if (null == bmp) {
			bmp = loadImageFromInternet(url, dir, name, 1);
		}
		
		return bmp;
	}
	
	/**
	 * Download image from Internet, convert to Bitmap data and store in cache folder
	 * also you can specified quality of image.
	 * 
	 * @param strUrl URL of image.
	 * @param dir Cache folder.
	 * @param filename Saved filename.
	 * @param sampleSize Quality of image, (1 = same as source) see BitmapFactory class.
	 * 
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromInternet(String strUrl, File dir, String filename, int sampleSize) {
		if (strUrl == null || dir == null || filename == null || sampleSize <= 0) {
			return null;
		}
		
		HttpURLConnection http = null;
		HttpsURLConnection https = null;
		
		Bitmap bmp = null;
		
		if (LOG) Log.i(TAG, "download image from " + strUrl);
		
		// try to connect to server
		try {
			// download image from Internet
			URL url = new URL(strUrl);
			InputStream inStream;
			
			if (strUrl.startsWith("http://")) {
				http = (HttpURLConnection) url.openConnection();
				http.setUseCaches(true);
				http.setConnectTimeout(TIME_OUT);
				inStream = new BufferedInputStream(http.getInputStream());
			}
			else if (strUrl.startsWith("https://")) {
				https = (HttpsURLConnection) url.openConnection();
				https.setUseCaches(true);
				https.setConnectTimeout(TIME_OUT);
				inStream = new BufferedInputStream(https.getInputStream());
			}
			else {
				return null;
			}
			
			// prepare file saving
			final File savedFile = new File(dir.getAbsolutePath() + "/" + filename);
			FileOutputStream fileStream = new FileOutputStream(savedFile);
			
			if (LOG) Log.i(TAG, "save image to " + savedFile.toString());
			
			// prepare bitmap decoding
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.RGB_565;
			options.inSampleSize = sampleSize;
			
			// read from input stream save to file and store for decode
			final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[16384];
			
			while ((nRead = inStream.read(data)) != -1) {
				byteStream.write(data, 0, nRead);

				fileStream.write(data, 0, nRead);
			}
			
			// close stream
			inStream.close();
			

            fileStream.flush();
            fileStream.close();
			
			// prepare byte stream for decode
			byteStream.flush();
			
			final byte[] imgByte = byteStream.toByteArray();
			
			byteStream.close();
			
			bmp = BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length, options);
		}
		catch (MalformedURLException e) {
			if (LOG) Log.e(TAG, "MalformedURL: " + e.toString());
		}
		catch (IOException e) {
			if (LOG) Log.e(TAG, "IO Error: " + e.toString());
		}
		catch (Exception e) {
			if (LOG) Log.e(TAG, "Unknown Error: " + e.toString());
		}
		finally {
			if (http != null) {
				http.disconnect();
			}
			if (https != null) {
				https.disconnect();
			}
		}
		
		return bmp;
	}
	
	/**
	 * Load image from cache folder instead of Internet and specified quality of image.
	 * 
	 * @param dir Cache folder.
	 * @param filename Name of file
	 * @param sampleSize Quality of image, (1 = same as source) see BitmapFactory class.
	 * 
	 * @return Converted Bitmap.
	 */
	public static Bitmap loadImageFromStorage(File dir, String filename, int sampleSize) {
		final String imgFile = dir.getAbsolutePath() + "/" + filename;

        // check if file exist
        File f = new File(imgFile);
        if (!f.exists() || f.isDirectory()) {
            return null;
        }
		
		// prepare bitmap decoding
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		options.inSampleSize = sampleSize;

		return BitmapFactory.decodeFile(imgFile, options);
	}
	
	/**
	 * Check network connection and set visibility of buttons on activity
	 */
	public static boolean isConnected(Context c) {
		final ConnectivityManager conMgr = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnected();
	}
}

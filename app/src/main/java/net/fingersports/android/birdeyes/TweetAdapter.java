package net.fingersports.android.birdeyes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ThreadPoolExecutor;

import twitter4j.Status;

public class TweetAdapter extends ArrayAdapter<Status> {
    private static final boolean LOG = Const.LOG;
    private static final String TAG = "ADAPTER";

	private final CopyOnWriteArrayList<Status> mTweets;
	private final LruCache<String, Bitmap> mAvatarCache;
	private final LruCache<Long, Bitmap> mImageCache;
    private static File mAvatarCacheDir;
    private static File mImageCacheDir;

    private static int mLimitMemSize;

    private static int mOldPos = -2;

    private static ThreadPoolExecutor mExecutor;
    private static Thread mImageLoader;
    private static ImageLoaderService mImageLoaderService;

	private final LayoutInflater mInflater;
	
	public TweetAdapter(Activity activity,
                        int textViewResourceId,
                        CopyOnWriteArrayList<Status> items,
                        LruCache<String, Bitmap> avatarCache,
                        LruCache<Long, Bitmap> imgCache,
                        File avtCacheDir,
                        File imgCacheDir,
                        ThreadPoolExecutor exec) {
		super(activity, textViewResourceId, items);
		this.mTweets = items;
		this.mAvatarCache = avatarCache;
		this.mImageCache = imgCache;
        this.mImageCacheDir = imgCacheDir;
        this.mAvatarCacheDir = avtCacheDir;
        this.mExecutor = exec;

		mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mLimitMemSize = ((mImageCache.maxSize() / 1024) / 1024) * 2;
        if (LOG) Log.i(TAG, "limit memory usage to " + mLimitMemSize + "MB");

        mImageLoaderService = new ImageLoaderService(activity, mAvatarCache, mImageCache, mAvatarCacheDir, mImageCacheDir, this, mExecutor);
	}

    private void cacheManagement(int pos, WeakReference<ImageView> iv) {
        Log.d(TAG, "pos: " + pos + ", old pos: " + mOldPos);

        if (LOG) Log.i(TAG, "tweets count: " + mTweets.size());

        if (pos > mOldPos) {
            // forward loading
            if (LOG) Log.i(TAG, "Forward Loading");

            int size = mTweets.size();
            for (int i = 0; i <= Const.LOAD_IMAGE_IN_ADVANCE; i++) {
                int j = pos + i;
                if (j < size) {
                    loadImage(mTweets.get(j), iv);
                    removeImage(j - mLimitMemSize);
                }
                else {
                    break;
                }
            }
        }
        else {
            // backward loading
            if (LOG) Log.i(TAG, "Backward Loading");

            for (int i = 0; i <= Const.LOAD_IMAGE_IN_ADVANCE; i++) {
                int j = pos - i;
                if (j >= 0) {
                    loadImage(mTweets.get(j), iv);
                    removeImage(j + mLimitMemSize);
                }
                else {
                    break;
                }
            }
        }

        mOldPos = pos;
    }

    public void loadImage(Status tweet, WeakReference<ImageView> iv) {
        if (mImageCache.get(tweet.getMediaEntities()[0].getId()) != null) { return; }

        mImageLoaderService.setUp(tweet, iv);
        mImageLoader = new Thread(mImageLoaderService);
        mImageLoader.start();
    }

    public void removeImage(Status tweet) {
        Long key = tweet.getMediaEntities()[0].getId();

        if (mImageCache.get(key) == null) { return; }

        mImageCache.remove(key);
    }

    public void removeImage(int i) {
        if (LOG) Log.d(TAG, "remove tweet no. " + i + "\'s image");
        if ((i < 0) || (i >= mTweets.size())) {
            return;
        }

        Long key = mTweets.get(i).getMediaEntities()[0].getId();
        if (mImageCache.get(key) != null) {
            mImageCache.remove(key);
        }
    }
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.part_status, null);
			
			holder = new ViewHolder();
			holder.avatar = (ImageView) convertView.findViewById(R.id.user_avatar);
			holder.user = (TextView) convertView.findViewById(R.id.user_name);
			holder.time = (TextView) convertView.findViewById(R.id.status_time);
			holder.image = (ImageView) convertView.findViewById(R.id.status_image);
			holder.message = (TextView) convertView.findViewById(R.id.status_message);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Status status = mTweets.get(position);
		if (status == null) {
			return convertView;
		}

        WeakReference<ImageView> ivref = new WeakReference<ImageView>(holder.image);

        cacheManagement(position, ivref);
		
		String name = status.getUser().getScreenName();
		long id = status.getMediaEntities()[0].getId();
		
		holder.user.setText("@" + name);
		holder.avatar.setImageBitmap(getAvatar(name));
		holder.time.setText(DateFormat.format(Const.DATE_FORMAT, status.getCreatedAt()));
		holder.message.setText(status.getText());
		holder.image.setImageBitmap(getImage(id));
		// reset view bounds to current image
		holder.image.setAdjustViewBounds(false);
		holder.image.setAdjustViewBounds(true);
		
		return convertView;
	}
	
	private Bitmap getAvatar(String key) {
		return mAvatarCache.get(key);
	}
	
	private Bitmap getImage(Long key) {
		return mImageCache.get(key);
	}
	
	private static class ViewHolder {
		public ImageView avatar;
		public TextView user;
		public TextView time;
		public ImageView image;
		public TextView message;
	}
}

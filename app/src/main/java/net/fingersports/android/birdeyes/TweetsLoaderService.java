package net.fingersports.android.birdeyes;

import android.util.Log;
import android.widget.Toast;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

import twitter4j.MediaEntity;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class TweetsLoaderService implements Runnable {
	private static boolean LOG = Const.LOG;
	private static String TAG = "TweetsLoaderService";
	
	private static int mLimitDelay = Const.LOAD_TWEETS_DELAY_MIN;
	
	private int mCurrentPage = 1;
	private MainActivity mActivity;
	private CopyOnWriteArrayList<Status> mTweets;
	private Twitter mTwitter;
	
	public TweetsLoaderService(
			MainActivity activity,
			Twitter twitter,
			CopyOnWriteArrayList<Status> tweets) {
		mActivity = activity;
		mTwitter = twitter;
		mTweets = tweets;
	}
	@Override
	public void run() {
		boolean success = loadTweets();
		
		while(!success) {
			if (Thread.interrupted()) {
				break;
			}
			
			try {
				Thread.sleep(mLimitDelay * 1000 * 60);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			
			success = loadTweets();
		}
		
		// reset delay
		mLimitDelay = Const.LOAD_TWEETS_DELAY_MIN;
		
		mCurrentPage++;
		
		mActivity.doAfterLoadedNewTweets();
	}
	
	private boolean loadTweets() {
		Iterator<Status> walker;
		ResponseList<Status> tempList;
		Paging page = new Paging(mCurrentPage, Const.TWEETS_PER_PAGE);
		
		if (LOG) Log.i(TAG, "download tweet page " + page);

		try {
			tempList = mTwitter.getHomeTimeline(page);
		}
		catch (TwitterException e) {
			if (LOG) e.printStackTrace();
			if (e.getRateLimitStatus() != null) {
                int limit = e.getRateLimitStatus().getSecondsUntilReset() / 1000;

                if (limit <= 0) {
                    limit = 1;
                }

				mLimitDelay = mLimitDelay + limit;
				mActivity.runOnUiThread(new ToastRunnable(mActivity.getString(R.string.msg_reach_limit) + mLimitDelay + mActivity.getString(R.string.msg_reach_limit_suffix)));
				return false;
			}
			
			mActivity.runOnUiThread(new ToastRunnable(mActivity.getString(R.string.msg_unstable_connection)));
			return false;
		}
		
		Status tweet;
		MediaEntity[] media;
		int mediaCount;
		
		walker = tempList.iterator();
		while (walker.hasNext()) {
			tweet = walker.next();
			media = tweet.getMediaEntities();
			mediaCount = media.length;
			
			for (int i = 0; i < mediaCount; i++) {
				if (media[i].getType().equalsIgnoreCase("photo")) {
					mTweets.add(tweet);
					break;
				}
			}
		}

        return true;
	}
	
	private class ToastRunnable implements Runnable {
		private String mText;
		
		public ToastRunnable(String msg) {
			mText = msg;
		}
		@Override
		public void run() {
			Toast.makeText(mActivity, mText, Toast.LENGTH_SHORT).show();
		}
		
	}
}

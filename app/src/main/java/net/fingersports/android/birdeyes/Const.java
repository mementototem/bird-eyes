package net.fingersports.android.birdeyes;

import java.util.concurrent.TimeUnit;

public class Const {
	public final static boolean LOG = false;
	public final static String KEY_TOKEN = "twitter_access_token";
	public final static String KEY_SECRET = "twitter_access_secret";
	
	public final static String API_KEY = "4GP2M9i5SoIRZigq0EGkWw";
	public final static String API_SECRET = "qLTPu09RFxU1SssG0jIKUN5IOtn17gTWlQgN19TB2Dc";
	public final static String API_CALLBACK = "birdeyes://oauth";
	public final static String API_VERIFIER = "oauth_verifier";
	public final static String API_NATIVE_URI = "twitter://status?status_id=";
	public final static String API_BROWSE_URI = "https://twitter.com/%s/status/%d";


    public final static int LOGIN_NO_CONNECTION = 7001;
	public final static int LOGIN_HAVE_CONNECTION = 7002;
	public final static int LOGIN_LOAD_DATA = 7003;
	
	public final static int TWEETS_PER_PAGE = 50;
	public final static int LOAD_MORE_THRESHOLD = 5;
	public final static int LOAD_IMAGE_DELAY = 1000;
	public final static int LOAD_TWEETS_DELAY_MIN = 2;

    public final static int LOAD_IMAGE_IN_ADVANCE = 3;
	
	public final static int NUM_OF_CORES = Runtime.getRuntime().availableProcessors();
	public final static int KEEP_ALIVE_TIME = 1;
	public final static TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
	
	public final static String PHOTO_SIZE = ":large";
	public final static int DEFAULT_PIC_SIZE = 1024;

    public final static String DATE_FORMAT = "dd MMM, yyyy kk:mm";

    public final static int HAVE_TWITTER_APP = 200;
	public final static int NO_TWITTER_APP = 201;
	public final static int NOT_CHECK = 202;
}

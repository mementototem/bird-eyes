package net.fingersports.android.birdeyes;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.concurrent.ThreadPoolExecutor;

import twitter4j.Status;

public class ImageLoaderService implements Runnable {
	Activity mActivity;
	Status mTweet;
	LruCache<String, Bitmap> mAvatarCache;
	LruCache<Long, Bitmap> mImageCache;
	File mAvatarCacheDir;
	File mImageCacheDir;
	TweetAdapter mAdapter;
	ThreadPoolExecutor mExecutor;

    WeakReference<ImageView> mIvRef;
	
	public ImageLoaderService(
			Activity activity,
			LruCache<String, Bitmap> aCache, LruCache<Long, Bitmap> iCache,
			File aCacheDir, File iCacheDir,
			TweetAdapter adapter,
			ThreadPoolExecutor executor) {
		mActivity = activity;
		mAvatarCache = aCache;
		mImageCache = iCache;
		mAvatarCacheDir = aCacheDir;
		mImageCacheDir = iCacheDir;
		mAdapter = adapter;
		mExecutor = executor;
	}

    public void setUp(Status tweet, WeakReference<ImageView> iv) {
        mTweet = tweet;
        mIvRef = iv;
    }
	
	@Override
	public void run() {
		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

		mExecutor.execute(new ImageLoaderRunnable(mActivity, mTweet, mAvatarCache, mImageCache, mAvatarCacheDir, mImageCacheDir, mAdapter, mIvRef));
	}
}

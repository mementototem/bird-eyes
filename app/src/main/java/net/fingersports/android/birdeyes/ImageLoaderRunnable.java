package net.fingersports.android.birdeyes;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.io.File;
import java.lang.ref.WeakReference;

import twitter4j.Status;

public class ImageLoaderRunnable implements Runnable {
	Activity mActivity;
	Status mTweet;
	LruCache<String, Bitmap> mAvatarCache;
	LruCache<Long, Bitmap> mImageCache;
	File mAvatarCacheDir;
	File mImageCacheDir;
	BaseAdapter mAdapter;
    WeakReference<ImageView> mIvRef;
	
	public ImageLoaderRunnable(
			Activity activity,
			Status tweet,
			LruCache<String, Bitmap> aCache, LruCache<Long, Bitmap> iCache,
			File aCacheDir, File iCacheDir,
			BaseAdapter adapter,
            WeakReference<ImageView> iv) {
		mActivity = activity;
		mTweet = tweet;
		mAvatarCache = aCache;
		mImageCache = iCache;
		mAvatarCacheDir = aCacheDir;
		mImageCacheDir = iCacheDir;
		mAdapter = adapter;
        mIvRef = iv;
	}
	
	@Override
	public void run() {
		Bitmap avatarBmp;
		Bitmap imgBmp;
		boolean sleep = false;
		
		String name = mTweet.getUser().getScreenName();
		String avatarUrl = mTweet.getUser().getProfileImageURLHttps();
						
		if (mAvatarCache.get(name) == null) {
			avatarBmp = Loader.loadImage(avatarUrl, name, mAvatarCacheDir);
			mAvatarCache.put(name, avatarBmp);
			
			sleep = true;
		}
		
		//TODO: Twitter enabled user to upload 4 photos per tweet, must handle this.
		Long imgId = mTweet.getMediaEntities()[0].getId();
		String imgUrl = mTweet.getMediaEntities()[0].getMediaURLHttps() + Const.PHOTO_SIZE;
		
		if (mImageCache.get(imgId) == null) {
			imgBmp = Loader.loadImage(imgUrl, imgId.toString(), mImageCacheDir);
			mImageCache.put(imgId, imgBmp);
			
			sleep = true;
		}
		
		mActivity.runOnUiThread(new NotifyAdapterRunnable());
		
		if (sleep) {
			try {
				Thread.sleep(Const.LOAD_IMAGE_DELAY);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			sleep = false;
		}
	}
	
	private class NotifyAdapterRunnable implements Runnable {
		
		@Override
		public void run() {
			mAdapter.notifyDataSetChanged();

            if (mIvRef != null) {
                ImageView iv = mIvRef.get();
                if (iv != null) {
                    iv.setImageBitmap(mImageCache.get(mTweet.getMediaEntities()[0].getId()));
                }
            }
		}
	}
}
